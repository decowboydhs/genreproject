# GenreProject

## Note
This readme and the requirements files specify versions for each dependency,
which were the latest stable releases at the time.
You can try using newer releases as well, but those may or may not work.

## Getting started
Before continuing, install _Python 3.6.4_ and _pip 19.2.3_.

### Preparations
On a computer where you will be running preparations, run:
`pip install -r requirements-preparations.txt`

Then install _FFMPEG 4.2.2_ from its [website](https://www.ffmpeg.org/download.html).
Verify that the correct _FFMPEG_ is available to _Python_ by running `ffmpeg` on the command-line and checking the version information.

### Experiments
On a computer where you will be running experiments, first run:
`pip install -r requirements-experiments.txt`

Then install _CUDA Toolkit 10.1_ from its [website](https://developer.nvidia.com/cuda-downloads),
and install _PyTorch 1.5.0_ and _TorchVision 0.6.0_ from their [website](https://pytorch.org/get-started/locally/).

Alternatively, a Peregrine job for running experiments with all dependencies automatically installed is available at `project_root/newcode/job`.

## Important
Use `project_root/newcode` as your working directory when executing scripts.

## Preparations
Download the _fma_medium_ dataset and the _fma_metadata_ from their [repository](https://github.com/mdeff/fma).
Extract these into the folders `project_root/fma_medium` and `project_root/fma_metadata`.
See the `instructions.txt` in those folders for more information.

Run **`prepare_audio.py`** to prepare the dataset.
This will check for corrupted audio files, perform normalization, convert the sample rate and more.
Results will be stored as `project_root/fma_medium_prepared/*.wav`.

Run **`parse_audio.py`** to preprocess the dataset.
This will split the audio files, calculate spectrograms and more.
Results will be stored as `project_root/fma_medium_parsed/*.*`.

These preparations only have to be run once.

### Migration
If you will be running experiments on a computer different from where you ran preparations (such as a Peregrine node), copy the following files to that computer:

 - `project_root/fma_medium_parsed/*.*`

## Running experiments
Run **`run_experiments.py`** to run experiments.

The experiments and their parameters are defined inside that script.

Settings that you may want to differ depending on the computer you're using can be modified using command-line arguments.
See `project_root/newcode/config/config.py` to view those arguments and their default values.
