import pandas
import numpy
import matplotlib
import os
import matplotlib.font_manager as fm
import matplotlib.pyplot as plt
import statistics
import seaborn
import scipy.stats
import librosa
import librosa.display
from matplotlib.patches import Rectangle


def get_statistics(experiment_ids, genre_name, measurement_name):
    result = []

    for experiment_id in experiment_ids:
        df = pandas.read_csv('../output/1000/' + str(experiment_id) + '.score')
        result.append(df.loc[df['genre'] == genre_name][measurement_name].iloc[0])

    return result


def get_confusion(experiment_ids, genre_names):
    result = numpy.zeros((len(genre_names), len(genre_names)), dtype=numpy.uint32)

    for experiment_id in experiment_ids:
        df = pandas.read_csv('../output/1000/' + str(experiment_id) + '.confusion')
        for row_index, row in df.iterrows():
            columns = list(row)
            columns.pop(0)
            for col_index in range(len(columns)):
                result[row_index][col_index] += columns[col_index]

    return result


def get_history_old(experiment_ids, experiment_times, polynome_dimension=4):
    poly = numpy.zeros(polynome_dimension + 1, numpy.float)

    time = 0
    for idx in range(len(experiment_ids)):
        experiment_id = experiment_ids[idx]
        experiment_time = experiment_times[idx]

        df = pandas.read_csv('../output/' + str(experiment_id) + '.history')
        x = df['epoch'].to_numpy()
        y = df['accuracy'].to_numpy()

        x = x * (experiment_time / x.size)

        z = numpy.polynomial.polynomial.polyfit(x, y, polynome_dimension)
        poly += z
        time += x[x.size - 250]

    poly = poly / len(experiment_ids)
    time = time / len(experiment_ids)

    f = numpy.polynomial.polynomial.Polynomial(poly)

    x_new = numpy.linspace(0, time, int(time))
    y_new = f(x_new)

    return x_new, y_new


def get_history(experiment_ids, experiment_times, polynome_dimension=8):
    data_x = numpy.zeros(0, numpy.float)
    data_y = numpy.zeros(0, numpy.float)

    time = 0
    for idx in range(len(experiment_ids)):
        experiment_id = experiment_ids[idx]
        experiment_time = experiment_times[idx]

        df = pandas.read_csv('../output/' + str(experiment_id) + '.history')
        x = df['epoch'].to_numpy()
        y = df['accuracy'].to_numpy()

        x = x * (experiment_time / x.size)

        data_x = numpy.append(data_x, x)
        data_y = numpy.append(data_y, y)
        time += x[x.size - 250]

    time = time / len(experiment_ids)

    poly = numpy.polynomial.polynomial.polyfit(data_x, data_y, polynome_dimension)
    f = numpy.polynomial.polynomial.Polynomial(poly)

    x_new = numpy.linspace(0, time, int(time))
    y_new = f(x_new)

    return x_new, y_new


def get_training_epochs(experiment_ids):
    for idx in range(len(experiment_ids)):
        experiment_id = experiment_ids[idx]

        df = pandas.read_csv('../output/' + str(experiment_id) + '.history')
        x = df['epoch'].to_numpy()

        print(x.size - 250)


def style_plot(plot, figure=None, axis=None, legend=None):
    plot.grid(b=None)
    plot.yticks(fontsize=13)
    plot.xticks(fontsize=13)

    if axis is not None:
        axis.grid(False)
        axis.set_facecolor("#FFFFFF")
        axis.tick_params(axis='x', bottom=True, length=5, width=1, direction='out', color='black')
        axis.tick_params(axis='y', left=True, length=5, width=1, direction='out', color='black')
        axis.spines['right'].set_visible(True)
        axis.spines['top'].set_visible(True)
        axis.spines['left'].set_visible(True)
        axis.spines['bottom'].set_visible(True)
        axis.spines['bottom'].set_color('black')
        axis.spines['left'].set_color('black')
        axis.spines['top'].set_color('black')
        axis.spines['right'].set_color('black')
        axis.spines['bottom'].set_linewidth(1)
        axis.spines['left'].set_linewidth(1)
        axis.spines['top'].set_linewidth(1)
        axis.spines['right'].set_linewidth(1)

    if legend is not None:
        legend.get_frame().set_edgecolor('black')
        legend.get_frame().set_facecolor('none')



fpath = 'C:\\Windows\\Fonts\\cmunrm.ttf'
prop = fm.FontProperties(fname=fpath)
fname = prop.get_name()
#seaborn.set(font=fname)
matplotlib.rcParams['font.sans-serif'] = fname
matplotlib.rcParams['font.family'] = "sans-serif"
matplotlib.style.use('seaborn-notebook')

random_ids = [12221967, 12221966, 12221964, 12163608, 12163606]
consecutive_ids = [12163621, 12163620, 12163619, 12163618, 12163617]
duplicate_ids = [12163616, 12163615, 12163614, 12163613, 12163612]
original_random_ids = [12163606, 12163608, 12221964, 12221966, 12221967]
original_consecutive_ids = [12163617, 12163618, 12163619, 12163620, 12163621]
original_duplicate_ids = [12163612, 12163613, 12163614, 12163615, 12163616]
random_times = [792, 1377, 797, 721, 952]
consecutive_times = [799, 917, 791, 748, 1005]
duplicate_times = [3486, 1623, 1486, 1833, 1988]

metadata = pandas.read_csv("../fma_medium_parsed/fma_medium_parsed.csv")
genres = sorted(metadata['genre'].unique())


microf_random = get_statistics(random_ids, 'Micro Average', 'f_score')
macrof_random = get_statistics(random_ids, 'Macro Average', 'f_score')
weightedf_random = get_statistics(random_ids, 'Weighted Average', 'f_score')

microf_consecutive = get_statistics(consecutive_ids, 'Micro Average', 'f_score')
macrof_consecutive = get_statistics(consecutive_ids, 'Macro Average', 'f_score')
weightedf_consecutive = get_statistics(consecutive_ids, 'Weighted Average', 'f_score')

microf_duplicate = get_statistics(duplicate_ids, 'Micro Average', 'f_score')
macrof_duplicate = get_statistics(duplicate_ids, 'Macro Average', 'f_score')
weightedf_duplicate = get_statistics(duplicate_ids, 'Weighted Average', 'f_score')


microf_random_mean = statistics.mean(microf_random) / 100
macrof_random_mean = statistics.mean(macrof_random) / 100
weightedf_random_mean = statistics.mean(weightedf_random) / 100

microf_consecutive_mean = statistics.mean(microf_consecutive) / 100
macrof_consecutive_mean = statistics.mean(macrof_consecutive) / 100
weightedf_consecutive_mean = statistics.mean(weightedf_consecutive) / 100

microf_duplicate_mean = statistics.mean(microf_duplicate) / 100
macrof_duplicate_mean = statistics.mean(macrof_duplicate) / 100
weightedf_duplicate_mean = statistics.mean(weightedf_duplicate) / 100


microf_random_sd = scipy.stats.sem(microf_random) / 100
macrof_random_sd = scipy.stats.sem(macrof_random) / 100
weightedf_random_sd = scipy.stats.sem(weightedf_random) / 100

microf_consecutive_sd = scipy.stats.sem(microf_consecutive) / 100
macrof_consecutive_sd = scipy.stats.sem(macrof_consecutive) / 100
weightedf_consecutive_sd = scipy.stats.sem(weightedf_consecutive) / 100

microf_duplicate_sd = scipy.stats.sem(microf_duplicate) / 100
macrof_duplicate_sd = scipy.stats.sem(macrof_duplicate) / 100
weightedf_duplicate_sd = scipy.stats.sem(weightedf_duplicate) / 100


N = 3
random_means = (microf_random_mean, macrof_random_mean, weightedf_random_mean)
consecutive_means = (microf_consecutive_mean, macrof_consecutive_mean, weightedf_consecutive_mean)
duplicate_means = (microf_duplicate_mean, macrof_duplicate_mean, weightedf_duplicate_mean)
random_std = (microf_random_sd, macrof_random_sd, weightedf_random_sd)
consecutive_std = (microf_consecutive_sd, macrof_consecutive_sd, weightedf_consecutive_sd)
duplicate_std = (microf_duplicate_sd, macrof_duplicate_sd, weightedf_duplicate_sd)
ind = numpy.arange(N)    # the x locations for the groups
width = 0.25       # the width of the bars: can also be len(x) sequence
kw = dict(lw=1.1)

# matplotlib.rcParams.update({'errorbar.elinewidth': 2})
fig, ax = plt.subplots()
p1 = plt.bar(ind - width, random_means, width*0.85, yerr=random_std, color='mediumvioletred', error_kw=kw)
p2 = plt.bar(ind, consecutive_means, width*0.85, yerr=consecutive_std, error_kw=kw)
p3 = plt.bar(ind + width, duplicate_means, width*0.85, yerr=duplicate_std, error_kw=kw)

plt.ylabel('F-score', labelpad=15)
#plt.title('F-Scores per experiment')
plt.xticks(ind, ('Micro', 'Macro', 'Weighted'))
plt.yticks(numpy.arange(0, 1.1, 0.10))
ax.set_ylim([0, 1])
legend = plt.legend((p1[0], p2[0], p3[0]), ('Random', 'Consecutive', 'Duplicate'))
style_plot(plt, fig, ax, legend)
fig.tight_layout()
plt.savefig("../output/fscores.png", dpi=300)
plt.close(fig)



class_trackcount = numpy.zeros((len(genres)), dtype=numpy.uint32)
for index, genre in enumerate(genres):
    class_trackcount[index] = len(metadata.loc[metadata.genre == genre]['track_id'].unique())


fig, ax = plt.subplots()
ax.bar(genres, class_trackcount, width=0.7)
plt.ylabel('Number of songs', labelpad=15)
plt.yticks(numpy.arange(0, 8000, 1000))
ax.set_ylim([0, 7750])
#plt.title('Songs per genre')
plt.xticks(rotation=45, ha="right")

for index, genre in enumerate(genres):
    data = class_trackcount[index]
    plt.text(x=index, y=data+100, s=f"{data}", fontdict=dict(fontsize=10.5), ha='center')

style_plot(plt, fig, ax)
fig.tight_layout()
plt.savefig("../output/songcounts.png", dpi=300)
plt.close(fig)

random_class_f = numpy.zeros((len(genres), len(random_ids)), dtype=numpy.float)
consecutive_class_f = numpy.zeros((len(genres), len(consecutive_ids)), dtype=numpy.float)
duplicate_class_f = numpy.zeros((len(genres), len(duplicate_ids)), dtype=numpy.float)
for index, genre in enumerate(genres):
    random_class_f[index] = get_statistics(random_ids, genre, 'f_score')
    consecutive_class_f[index] = get_statistics(consecutive_ids, genre, 'f_score')
    duplicate_class_f[index] = get_statistics(duplicate_ids, genre, 'f_score')

random_class_f = random_class_f / 100
consecutive_class_f = consecutive_class_f / 100
duplicate_class_f = duplicate_class_f / 100

random_class_mean_f = numpy.mean(random_class_f, axis=1)
consecutive_class_mean_f = numpy.mean(consecutive_class_f, axis=1)
duplicate_class_mean_f = numpy.mean(duplicate_class_f, axis=1)

random_class_std_f = scipy.stats.sem(random_class_f, axis=1)
duplicate_class_std_f = scipy.stats.sem(consecutive_class_f, axis=1)
consecutive_class_std_f = scipy.stats.sem(duplicate_class_f, axis=1)

fig, ax = plt.subplots()
ind = numpy.arange(len(genres))
p1 = plt.bar(ind - width, tuple(random_class_mean_f), width, yerr=tuple(random_class_std_f), color='mediumvioletred', error_kw=kw)
p2 = plt.bar(ind, tuple(consecutive_class_mean_f), width, yerr=tuple(duplicate_class_std_f), error_kw=kw)
p3 = plt.bar(ind + width, tuple(duplicate_class_mean_f), width, yerr=tuple(consecutive_class_std_f), error_kw=kw)

plt.ylabel('F-Score', labelpad=15)
#plt.title('Songs per genre')
plt.xticks(range(len(genres)), genres, rotation=45, ha="right")
plt.yticks(numpy.arange(0, 1.1, 0.10))
ax.set_ylim([0, 1])
legend = plt.legend((p1[0], p2[0], p3[0]), ('Random', 'Consecutive', 'Duplicate'))
style_plot(plt, fig, ax, legend)
fig.tight_layout()
plt.savefig("../output/fscoregenres.png", dpi=300)
plt.close(fig)





def confusion_matrix(ids, name):
    array = get_confusion(ids, genres)

    df_cm = pandas.DataFrame(array, genres, genres)
    df_cmn = df_cm.astype('float') / df_cm.sum(axis=1)[:, numpy.newaxis]
    # df_cmn = df_cmn.where(df_cmn >= 0.005, numpy.nan)
    mask = df_cmn.isnull()
    # plt.figure(figsize=(10,7))
    seaborn.set(font_scale=0.8) # for label size
    seaborn.set_style({'font.sans-serif': fname})
    hm = seaborn.heatmap(df_cmn, annot=True, annot_kws={"size": 6.5}, cmap='Blues', fmt='.2f', mask=mask)
    #plt.xticks(range(len(genres)), genres, rotation=45, ha="right")
    plt.xticks(rotation=45, ha="right")
    style_plot(plt, None, plt.gca())
    plt.yticks(fontsize=10)
    plt.xticks(fontsize=10)

    for i in range(len(genres)):
        plt.gca().add_patch(Rectangle((i, i), 1, 1, fill=False, edgecolor='black', lw=1))
    # hm.set_facecolor('#f6faff')
    plt.tight_layout()
    plt.savefig("../output/confusion_" + name + ".png", dpi=300)
    plt.close(plt.gcf())


confusion_matrix(random_ids, "random")
confusion_matrix(duplicate_ids, "duplicate")
confusion_matrix(consecutive_ids, "consecutive")



random_x, random_y = get_history(original_random_ids, random_times)
consecutive_x, consecutive_y = get_history(original_consecutive_ids, consecutive_times)
duplicate_x, duplicate_y = get_history(original_duplicate_ids, duplicate_times)


fig, ax = plt.subplots()
p1 = ax.plot(random_x, random_y)
p2 = ax.plot(consecutive_x, consecutive_y)
p3 = ax.plot(duplicate_x, duplicate_y)

plt.ylabel('Accuracy (%)', labelpad=15)
#plt.title('F-Scores per experiment')
plt.yticks(numpy.arange(0, 101, 10))
ax.set_ylim([0, 100])
ax.set_xlim([0, 1590])
legend = plt.legend((p1[0], p2[0], p3[0]), ('Random', 'Consecutive', 'Duplicate'))
style_plot(plt, fig, ax, legend)
fig.tight_layout()
plt.savefig("../output/times.png", dpi=300)
plt.close(fig)


fig, ax = plt.subplots()
#y, sr = librosa.load("../fma_medium_prepared/009/009152.wav", duration=1)
y, sr = librosa.load("../fma_medium_prepared/009/009155.wav", duration=1)
sample, _ = librosa.effects.trim(y)
n_fft = 1024
hop_length = 512
n_mels = 128
S = librosa.feature.melspectrogram(sample, sr=sr, n_fft=n_fft, hop_length=hop_length, n_mels=n_mels)
S_DB = librosa.power_to_db(S, ref=numpy.max)
librosa.display.specshow(S_DB, sr=sr, hop_length=hop_length, x_axis='time', y_axis='mel')
cbar = plt.colorbar(format='%+2.0f dB')
cbar.ax.tick_params(size=0)
plt.ylabel('Frequency (Hz)', labelpad=15)
plt.xlabel('Time (s)', labelpad=10)
style_plot(plt, fig, ax)
fig.tight_layout()
plt.savefig("../output/mel.png", dpi=300)
plt.close(fig)
