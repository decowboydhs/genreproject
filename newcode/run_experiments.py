data_parsed_file = '../fma_medium_parsed/fma_medium_parsed'


if __name__ == '__main__':
    import pyfiglet
    ascii_banner = pyfiglet.figlet_format("GenreProject")
    print(ascii_banner, flush=True)
    print("", flush=True)

    import config.config as config
    import config.experiments as experiments
    print("Current settings:")
    print(config.args)
    print("", flush=True)
    print("", flush=True)

    from models.datasource import DataSource

    print("Preparing data source:", flush=True)
    datasource = DataSource(data_parsed_file + ".npy", data_parsed_file + ".csv")
    datasource.define_datacollection("train", 50)
    datasource.define_datacollection("test", 25)
    datasource.define_datacollection("validate", 25)
    datasource.prepare()
    print("Done!", flush=True)
    print("", flush=True)

    print("Preparing experiment '" + config.args.experiment + "':", flush=True)
    neuralnet = experiments.prepare_experiment(config.args.experiment, datasource)
    print("Done!", flush=True)
    print("", flush=True)

    print("Training neural networks:", flush=True)
    neuralnet.train("train", "validate", learning_rate=0.0001, learning_rate_half_time=None, early_stopping_patience=250)
    print("Done!", flush=True)
    print("", flush=True)

    print("Extended testing neural networks:", flush=True)
    neuralnet.test_extended("test")
    print("Done!", flush=True)
    print("", flush=True)

    print("Saving neural networks:", flush=True)
    neuralnet.save(config.args.name)
    print("Done!", flush=True)
    print("", flush=True)
