import torch.hub
import torch.utils.data
import torch.backends.cudnn
import math
import numpy
import config.config as config
import copy
import pandas
import pickle
import sys
import statistics


class NeuralNet:
    def __init__(self, datasource, pretrained, duplicatesamples, consecutivesamples, epochs_validate_multiplier=1, epochs_validate_skip=0):
        torch.hub.set_dir('../cache')

        self.dataloaders = {}
        self.datasets = {}
        self.duplicatesamples = duplicatesamples
        self.consecutivesamples = consecutivesamples
        self.epochs_validate_multiplier = epochs_validate_multiplier
        self.epochs_validate_skip = epochs_validate_skip

        self.datasource = datasource
        self.history = pandas.DataFrame(None, columns=['epoch', 'learning_rate', 'accuracy', 'loss',
                                                       'early_stopping_counter'])
        self.score = pandas.DataFrame(None, columns=['genre', 'true_positives', 'false_positives', 'false_negatives',
                                                     'precision', 'recall', 'f_score'])
        self.confusion = pandas.DataFrame(None, columns=(['genre'] + self.datasource.classes))

        print("- Preparing device...", end='', flush=True)
        self.device = self.get_device()
        if self.device == "cuda:0":
            torch.backends.cudnn.benchmark = True
            torch.backends.cudnn.enabled = True
        print(" 100%", flush=True)

        print("- Loading structure of type '" + self.__class__.__name__ + "'...", end='', flush=True)
        self.structure = self.get_structure(datasource=self.datasource, pretrained=pretrained)
        self.structure = self.structure.to(self.device)
        self.structure_best = None
        print(" 100%", flush=True)

        print("- Setting pretrained = '" + str(pretrained) + "'... 100%", flush=True)
        print("- Setting duplicatesamples = '" + str(duplicatesamples) + "'... 100%", flush=True)
        print("- Setting consecutivesamples = '" + str(consecutivesamples) + "'... 100%", flush=True)
        print("- Setting epochs_validate_multiplier = '" + str(epochs_validate_multiplier) + "'... 100%", flush=True)

    def get_structure(self, datasource, pretrained):
        raise Exception("Structure for neural net not yet implemented!")

    def get_dataset(self, datasource, datacollection_name, seed):
        raise Exception("Data set for neural net not yet implemented!")

    def get_sample_transform(self):
        raise Exception("Transform for neural net not yet implemented!")

    def get_batch_transform(self):
        raise Exception("Transform for neural net not yet implemented!")

    def get_dataloader(self, datacollection_name, batch_size, shuffle, seed, drop_last):
        if datacollection_name not in self.dataloaders:
            is_windows = sys.platform.startswith('win')

            self.dataloaders[datacollection_name] = torch.utils.data.DataLoader(
                self.get_dataset(datasource=self.datasource, datacollection_name=datacollection_name, seed=seed),
                batch_size=batch_size, shuffle=shuffle, num_workers=(0 if is_windows else config.args.num_workers),
                pin_memory=True, drop_last=drop_last, collate_fn=self.collate)

        return self.dataloaders[datacollection_name]

    def get_device(self):
        if config.args.device != "auto":
            device_name = config.args.device
        else:
            use_cuda = torch.cuda.is_available()
            device_name = "cuda:0" if use_cuda else "cpu"

        print(" (", device_name, ")", end='', flush=True)
        return torch.device(device_name)

    def collate(self, batch):
        transposed = zip(*batch)

        tensor, genre, idx = transposed

        tensor = torch.stack(tensor, 0, out=None)
        genre = torch.stack(genre, 0, out=None)
        idx = torch.tensor(idx)

        transform = self.get_batch_transform()
        if transform is not None:
            tensor = transform(tensor)

        return tensor, genre, idx

    def train(self, datacollection_train_name, datacollection_validation_name, learning_rate, learning_rate_half_time,
              early_stopping_patience):
        print(
            "- Training network on '" + datacollection_train_name + "' / '" + datacollection_validation_name + "' with learning rate '" + str(
                learning_rate) + "'...", end='', flush=True)

        dataset = self.get_dataset(datasource=self.datasource, datacollection_name=datacollection_train_name,
                                   seed=False)
        dataloader = self.get_dataloader(datacollection_name=datacollection_train_name,
                                         batch_size=config.args.batch_size_train, shuffle=True, seed=False,
                                         drop_last=True)

        criterion = torch.nn.CrossEntropyLoss()
        optimizer = torch.optim.Adam(self.structure.parameters(), lr=learning_rate)

        early_stopping_score = None
        early_stopping_counter = 0
        last_progress = -1
        current = 0
        total = dataset.__len__() * config.args.max_epochs_train
        epoch_count = 0
        for epoch in range(config.args.max_epochs_train):
            self.structure.train()

            for i, data in enumerate(dataloader, 0):
                tensor, genre, idx = data
                tensor, genre = tensor.to(self.device, dtype=torch.float), genre.to(self.device, dtype=torch.long)

                outputs = self.structure(tensor)

                loss = criterion(outputs, genre)
                loss.backward()
                optimizer.step()

                if config.args.show_progress:
                    current += config.args.batch_size_train
                    progress = math.floor(current / total * 100)

                    if progress > last_progress:
                        last_progress = progress
                        print(".", end='', flush=True)

            if epoch_count >= self.epochs_validate_skip:
                new_early_stopping_score = self.validate(datacollection_validation_name)
                if early_stopping_score is None or \
                        new_early_stopping_score['accuracy'] > early_stopping_score['accuracy'] \
                        or new_early_stopping_score['loss'] < early_stopping_score['loss']:
                    self.structure_best = copy.deepcopy(self.structure)
                    early_stopping_score = new_early_stopping_score
                    early_stopping_counter = 0
                else:
                    early_stopping_counter += 1

                self.history.loc[len(self.history)] = [epoch_count, learning_rate, new_early_stopping_score['accuracy'],
                                                       new_early_stopping_score['loss'], early_stopping_counter]

            epoch_count += 1
            if learning_rate_half_time is not None and epoch_count % learning_rate_half_time == 0:
                learning_rate *= 0.5

            if early_stopping_patience is not None and early_stopping_counter >= early_stopping_patience:
                break

        print(" 100%", flush=True)

    def validate(self, datacollection_name):
        dataloader = self.get_dataloader(datacollection_name=datacollection_name,
                                         batch_size=config.args.batch_size_test, shuffle=False, seed=True,
                                         drop_last=False)

        criterion = torch.nn.CrossEntropyLoss()

        correct_answers = 0
        total_answers = 0

        total_loss = 0
        total_loss_count = 0

        with torch.no_grad():
            self.structure.eval()
            for epoch in range(config.args.epochs_validate * self.epochs_validate_multiplier):
                dataloader.dataset.set_epoch_index(epoch)

                for i, data in enumerate(dataloader, 0):
                    tensor, genre, idx = data
                    tensor, genre = tensor.to(self.device, dtype=torch.float), genre.to(self.device, dtype=torch.long)

                    outputs = self.structure(tensor)
                    loss = criterion(outputs, genre)

                    _, predicted = torch.max(outputs, 1)
                    predicted = predicted.squeeze()

                    for x in range(len(predicted)):
                        if genre[x] == predicted[x]:
                            correct_answers += 1
                        total_answers += 1

                    total_loss += loss.item() * len(predicted)
                    total_loss_count += len(predicted)

        return {'accuracy': correct_answers * 100.0 / total_answers, 'loss': total_loss / total_loss_count}

    def test(self, datacollection_name):
        print("- Testing network on '" + datacollection_name + "'...", end='', flush=True)
        dataset = self.get_dataset(datasource=self.datasource, datacollection_name=datacollection_name, seed=False)
        dataloader = self.get_dataloader(datacollection_name=datacollection_name,
                                         batch_size=config.args.batch_size_test, shuffle=False, seed=False,
                                         drop_last=False)

        votes = numpy.zeros((dataset.__len__(), len(self.datasource.classes)), dtype=numpy.uint32)
        answers = numpy.zeros((dataset.__len__()), dtype=numpy.uint8)
        class_correct = numpy.zeros((len(self.datasource.classes)), dtype=numpy.uint32)
        class_total = numpy.zeros((len(self.datasource.classes)), dtype=numpy.uint32)
        accuracy = numpy.zeros((len(self.datasource.classes)), dtype=numpy.float)

        last_progress = -1
        current = 0
        total = dataset.__len__() * config.args.epochs_test

        with torch.no_grad():
            self.structure.eval()
            for epoch in range(config.args.epochs_test):

                for i, data in enumerate(dataloader, 0):
                    tensor, genre, idx = data
                    tensor, genre = tensor.to(self.device, dtype=torch.float), genre.to(self.device, dtype=torch.long)

                    outputs = self.structure_best(tensor)

                    _, predicted = torch.max(outputs, 1)
                    predicted = predicted.squeeze()

                    for x in range(len(predicted)):
                        votes[idx[x]][predicted[x]] += 1
                        answers[idx[x]] = genre[x]

                    if config.args.show_progress:
                        current += config.args.batch_size_test
                        progress = math.floor(current / total * 100)

                        if progress > last_progress:
                            last_progress = progress
                            print(".", end='', flush=True)

        votes = votes.argmax(axis=1)
        for i in range(len(votes)):
            vote = votes[i]
            answer = answers[i]

            if vote == answer:
                class_correct[answer] += 1

            class_total[answer] += 1

        for i in range(len(dataset.datasource.classes)):
            if class_total[i] > 0.0:
                accuracy[i] = 100.0 * class_correct[i] / class_total[i]
            else:
                accuracy[i] = 0.0

        print(" 100%", flush=True)

        print("", flush=True)
        for i in range(len(dataset.datasource.classes)):
            print('Accuracy of %20s : %2d %%' % (
                dataset.datasource.classes[i], accuracy[i]))

    def test_extended(self, datacollection_name):
        print("- Extended testing network on '" + datacollection_name + "'...", end='', flush=True)
        dataset = self.get_dataset(datasource=self.datasource, datacollection_name=datacollection_name, seed=False)
        dataloader = self.get_dataloader(datacollection_name=datacollection_name,
                                         batch_size=config.args.batch_size_test, shuffle=False, seed=False,
                                         drop_last=False)

        votes = numpy.zeros((dataset.__len__(), len(self.datasource.classes)), dtype=numpy.float)
        answers = numpy.zeros((dataset.__len__()), dtype=numpy.uint8)
        class_count = numpy.zeros((len(self.datasource.classes)), dtype=numpy.uint32)
        true_positives = numpy.zeros((len(self.datasource.classes)), dtype=numpy.uint32)
        false_positives = numpy.zeros((len(self.datasource.classes)), dtype=numpy.uint32)
        false_negatives = numpy.zeros((len(self.datasource.classes)), dtype=numpy.uint32)
        confusion = numpy.zeros((len(self.datasource.classes), len(self.datasource.classes)), dtype=numpy.uint32)  # rows = actual, columns = predicted

        last_progress = -1
        current = 0
        total = dataset.__len__() * config.args.epochs_test

        with torch.no_grad():
            self.structure.eval()
            for epoch in range(config.args.epochs_test):

                for i, data in enumerate(dataloader, 0):
                    tensor, genre, idx = data
                    tensor, genre = tensor.to(self.device, dtype=torch.float), genre.to(self.device, dtype=torch.long)

                    outputs = self.structure_best(tensor)

                    values = outputs.data.cpu().numpy()
                    for x in range(values.shape[0]):
                        votes[idx[x]] += values[x]
                        answers[idx[x]] = genre[x]

                    if config.args.show_progress:
                        current += config.args.batch_size_test
                        progress = math.floor(current / total * 100)

                        if progress > last_progress:
                            last_progress = progress
                            print(".", end='', flush=True)

        predictions = numpy.argmax(votes, 1)
        for i in range(len(votes)):
            answer = answers[i]
            prediction = predictions[i]

            confusion[answer][prediction] += 1
            class_count[answer] += 1

            if prediction == answer:
                true_positives[answer] += 1
            else:
                false_positives[prediction] += 1
                false_negatives[answer] += 1

        precision = numpy.zeros((len(self.datasource.classes)), dtype=numpy.float)
        recall = numpy.zeros((len(self.datasource.classes)), dtype=numpy.float)
        fscore = numpy.zeros((len(self.datasource.classes)), dtype=numpy.float)

        for i in range(len(self.datasource.classes)):
            if true_positives[i] + false_positives[i] > 0:
                precision[i] = true_positives[i] / (true_positives[i] + false_positives[i])
            if true_positives[i] + false_negatives[i] > 0:
                recall[i] = true_positives[i] / (true_positives[i] + false_negatives[i])
            if precision[i] + recall[i] > 0:
                fscore[i] = 2 * ((precision[i] * recall[i]) / (precision[i] + recall[i]))

        precision = numpy.nan_to_num(precision)
        recall = numpy.nan_to_num(recall)
        fscore = numpy.nan_to_num(fscore)

        for i in range(len(self.datasource.classes)):
            self.score.loc[len(self.score)] = [self.datasource.classes[i], true_positives[i], false_positives[i],
                                               false_negatives[i],
                                               100 * precision[i], 100 * recall[i], 100 * fscore[i]]

        micro_precision = numpy.sum(true_positives) / (numpy.sum(true_positives) + numpy.sum(false_positives))
        micro_recall = numpy.sum(true_positives) / (numpy.sum(true_positives) + numpy.sum(false_negatives))
        micro_fscore = statistics.harmonic_mean([micro_precision, micro_recall])

        self.score.loc[len(self.score)] = ["Micro Average", numpy.sum(true_positives), numpy.sum(false_positives),
                                           numpy.sum(false_negatives),
                                           100 * micro_precision, 100 * micro_recall, 100 * micro_fscore]

        macro_precision = numpy.sum(precision) / len(self.datasource.classes)
        macro_recall = numpy.sum(recall) / len(self.datasource.classes)
        macro_fscore = numpy.sum(fscore) / len(self.datasource.classes)

        self.score.loc[len(self.score)] = ["Macro Average", numpy.sum(true_positives), numpy.sum(false_positives),
                                           numpy.sum(false_negatives),
                                           100 * macro_precision, 100 * macro_recall, 100 * macro_fscore]

        weighted_precision = numpy.sum(precision * class_count) / numpy.sum(class_count)
        weighted_recall = numpy.sum(recall * class_count) / numpy.sum(class_count)
        weighted_fscore = numpy.sum(fscore * class_count) / numpy.sum(class_count)

        self.score.loc[len(self.score)] = ["Weighted Average", numpy.sum(true_positives), numpy.sum(false_positives),
                                           numpy.sum(false_negatives),
                                           100 * weighted_precision, 100 * weighted_recall, 100 * weighted_fscore]

        for i in range(len(self.datasource.classes)):
            self.confusion.loc[len(self.confusion)] = [self.datasource.classes[i]] + confusion[i].tolist()

        print(" 100%", flush=True)
        print("", flush=True)

        with pandas.option_context('display.max_rows', None, 'display.max_columns', None, 'display.precision', 2, 'display.width', 1000):
            print(self.score)
            print("", flush=True)
            print(self.confusion)

    def save(self, name):
        output_name = '../output/' + name
        print("- Preparing output at '" + output_name + "'... 100%", flush=True)

        print("- Saving structure...", end='', flush=True)
        structure = self.structure_best.cpu()
        torch.save(structure, output_name + '_model.pth')
        torch.save(structure.state_dict(), output_name + '_parameters.pth')
        print(" 100%", flush=True)

        print("- Saving history...", end='', flush=True)
        self.history.to_csv(output_name + '.history', encoding='utf-8', index=False)
        print(" 100%", flush=True)

        print("- Saving score...", end='', flush=True)
        self.score.to_csv(output_name + '.score', encoding='utf-8', index=False)
        print(" 100%", flush=True)

        print("- Saving confusion matrix...", end='', flush=True)
        self.confusion.to_csv(output_name + '.confusion', encoding='utf-8', index=False)
        print(" 100%", flush=True)

        print("- Saving track list...", end='', flush=True)
        with open(output_name + '.tracklist', 'wb') as track_output:
            pickle.dump(self.datasource.track_list, track_output)
        print(" 100%", flush=True)

    def load(self, file):
        self.structure = torch.load(file)
        self.structure = self.structure.to(self.device)
        self.structure_best = copy.deepcopy(self.structure)
