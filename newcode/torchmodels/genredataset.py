import random
import numpy
import torch
from torch.utils.data import Dataset


class GenreDataset(Dataset):

    def __init__(self, datasource, datacollection_name, tensorsize=None, samplecount=3, consecutivesamples=False, duplicatesamples=False, glue=False, seed=False, transform=None):
        self.datacollection = datasource.datacollections[datacollection_name]
        self.datasource = datasource
        self.samplecount = samplecount
        self.consecutivesamples = consecutivesamples
        self.duplicatesamples = duplicatesamples
        self.glue = glue
        self.tensorsize = tensorsize
        self.transform = transform
        self.seed = seed
        self.random_seed_value = random.randint(0, self.__len__() * 30 * 29 * 28)
        self.epoch_index = 0

    def __len__(self):
        return len(self.datacollection.tracks)

    def set_epoch_index(self, epoch_index):
        self.epoch_index = epoch_index

    def __getitem__(self, idx):
        track = self.datacollection.tracks[idx]

        if self.seed:
            random.seed(self.random_seed_value * self.epoch_index * idx)

        if self.duplicatesamples:
            sample_index = random.randint(track.data_start, track.data_start + track.segment_count - 1)
            sample_list = [self.datasource.data[sample_index] for i in range(self.samplecount)]
        elif self.consecutivesamples:
            sample_index = random.randint(track.data_start, track.data_start + track.segment_count - self.samplecount)
            sample_list = [self.datasource.data[i] for i in range(sample_index, sample_index + self.samplecount)]
        else:
            sample_index = random.sample(range(track.data_start, track.data_start + track.segment_count), self.samplecount)
            sample_list = [self.datasource.data[i] for i in sample_index]

        sample_array = numpy.asarray(sample_list)

        if self.glue:
            sample_array = numpy.expand_dims(numpy.concatenate(sample_array, axis=1), axis=0)

        tensor = torch.from_numpy(sample_array)
        if self.tensorsize:
            tensor = tensor.unsqueeze(0)
            tensor = torch.nn.functional.interpolate(tensor, self.tensorsize, mode='bilinear', align_corners=True)
            tensor = tensor.squeeze(0)

        if self.transform:
            tensor = self.transform(tensor)

        genre = torch.tensor(self.datasource.classes.index(track.genre))

        return tensor, genre, idx

