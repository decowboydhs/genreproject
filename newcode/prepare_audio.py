data_mp3_dir = '../fma_medium/'
data_processed_dir = '../fma_medium_prepared/'


def list_files(dir):
    r = []
    subdirs = [x[0] for x in os.walk(dir)]
    for subdir in subdirs:
        files = os.walk(subdir).__next__()[2]
        if (len(files) > 0):
            for file in files:
                if (".mp3" in file):
                    r.append(subdir + "/" + file)
    return r


def create_hierarchy(input_dir, output_dir):
    input_subdirs = [x[0] for x in os.walk(input_dir)]
    for input_subdir in input_subdirs:
        output_subdir = input_subdir.replace(data_mp3_dir, data_processed_dir)
        if not os.path.exists(output_subdir):
            os.makedirs(output_subdir)


if __name__ == '__main__':
    import os
    import subprocess
    import ffmpeg
    import math

    if not os.path.exists(data_mp3_dir):
        raise ValueError('Could not find dataset.')
    if not os.path.exists(data_processed_dir):
        os.makedirs(data_processed_dir)

    create_hierarchy(data_mp3_dir, data_processed_dir)

    clips = list_files(data_mp3_dir)
    clips_count = len(clips)
    last_progress = -1

    for clip_index, clip in enumerate(clips):
        file = clip.replace(data_mp3_dir, data_processed_dir)
        file = file.replace('.mp3', '.wav')

        try:
            out, err = ffmpeg \
                .input(clip) \
                .output(file, format='wav', acodec='pcm_s16le', ac=1, ar='22050') \
                .global_args('-loglevel', 'warning') \
                .run(capture_stdout=True, capture_stderr=True, overwrite_output=True)

            if out or err:
                os.remove(file)
                print("Skipping due to warning: ", clip)
                continue

            normalize = subprocess.run('ffmpeg-normalize ' + file + ' -nt peak -t 0 -o ' + file + ' -f')

            if normalize.returncode != 0:
                os.remove(file)
                print("Skipping due to normalization error: ", clip)
                continue

        except ffmpeg._run.Error:
            print("Skipping due to error: ", clip)

        progress = math.floor(clip_index / clips_count * 100)

        if progress > last_progress:
            last_progress = progress
            print(progress, "%")
