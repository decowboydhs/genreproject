data_processed_dir = '../fma_medium_prepared/'
data_parsed_file = '../fma_medium_parsed/fma_medium_parsed'


def list_files(dir):
	r = []
	subdirs = [x[0] for x in os.walk(dir)]
	for subdir in subdirs:
		files = os.walk(subdir).__next__()[2]
		if len(files) > 0:
			for file in files:
				if ".wav" in file:
					r.append(subdir + "/" + file)
	return r


def parse_segment(segment_arr, segment_sr, segment_index):
	ms_t = librosa.feature.melspectrogram(segment_arr[segment_index*22050:(segment_index+1)*22050], sr=segment_sr, n_fft=1024, hop_length=512)
	ms_t = librosa.amplitude_to_db(ms_t)
	return ms_t
	
	
if __name__ == '__main__':
	import os
	import math
	import numpy
	import librosa
	import csv
	from fma_code import fma
	
	if not os.path.exists(data_processed_dir):
		raise ValueError('Could not find data location!')
	
	clips = list_files(data_processed_dir)
	clips_count = len(clips)
	last_progress = -1
	
	data = numpy.zeros((clips_count * 30, 128, 44))
	data_position = 0
	
	if os.path.exists(data_parsed_file + ".csv"):
		os.remove(data_parsed_file + ".csv")
	
	if os.path.exists(data_parsed_file + ".npy"):
		os.remove(data_parsed_file + ".npy")
		
	csv_file = open(data_parsed_file + ".csv", mode='w', newline='')
	csv_writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
	csv_writer.writerow(["position", "file", "track_id", "segment_index", "genre", "favorites", "listens", "interest"])
	
	metadata = fma.load('../fma_metadata/tracks.csv')
	
	for clip_index, clip in enumerate(clips):
		
		arr, sr = librosa.core.load(clip)
		segments = min(math.floor(len(arr) / sr), 30)
		
		track_id = os.path.basename(clip).replace('.wav', '').lstrip("0")
		track = metadata.query("track_id == '" + track_id + "'").iloc[0]['track']
		
		for i in range(segments):
			data[data_position] = parse_segment(arr, sr, i)
			csv_writer.writerow([data_position, clip, track_id, i, track['genre_top'], track['favorites'], track['listens'], track['interest']])
			data_position += 1
			
		progress = math.floor(clip_index / clips_count * 100)
		
		if progress > last_progress:
			last_progress = progress
			print(progress, "%")

	data.resize((data_position, 128, 44))
	
	numpy.save(data_parsed_file + ".npy", data)
