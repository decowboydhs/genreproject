from torchmodels.neuralnet import NeuralNet
import torch.hub
from torchmodels.genredataset import GenreDataset
import torchvision
import helpers.batch_transforms


class ResNet(NeuralNet):
    def get_dataset(self, datasource, datacollection_name, seed):
        if datacollection_name not in self.datasets:
            self.datasets[datacollection_name] = GenreDataset(datasource, datacollection_name=datacollection_name,
                                                              tensorsize=(224, 224),
                                                              samplecount=3, consecutivesamples=self.consecutivesamples,
                                                              duplicatesamples=self.duplicatesamples,
                                                              glue=False, seed=seed,
                                                              transform=self.get_sample_transform())

        return self.datasets[datacollection_name]

    def get_structure(self, datasource, pretrained):
        structure = torch.hub.load('pytorch/vision:v0.6.0', 'resnet50', pretrained=pretrained, verbose=False)
        structure.fc = torch.nn.Linear(2048, len(datasource.classes))
        return structure

    def get_sample_transform(self):
        return None

    def get_batch_transform(self):
        return torchvision.transforms.Compose([
            helpers.batch_transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225], inplace=True)
        ])
