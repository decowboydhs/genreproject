import argparse
import os

parser = argparse.ArgumentParser(description='GenreProject')

parser.add_argument('--experiment', type=str,
                    help='Name of the experiment', required=True)

parser.add_argument('--name', type=str,
                    help='Filename of the run', default=os.getenv('SLURM_JOB_ID', 'test'))

parser.add_argument('--device', type=str,
                    help='Device to use for calculations (auto, cpu, cuda:x)', default="auto")

parser.add_argument('--num_workers', type=int,
                    help='Number of data loader workers', default=0)

parser.add_argument('--batch_size_train', type=int,
                    help='Size of mini-batch during training', default=32)
parser.add_argument('--batch_size_test', type=int,
                    help='Size of mini-batch during testing / validating', default=128)

parser.add_argument('--max_epochs_train', type=int,
                    help='Maximum epochs for training', default=3000)
parser.add_argument('--epochs_test', type=int,
                    help='Epochs for testing', default=30)
parser.add_argument('--epochs_validate', type=int,
                    help='Epochs for validating', default=3)

parser.add_argument('--minimize_disk_io',
                    help='Keep data in memory to minimize disk IO', action='store_true')

parser.add_argument('--show_progress',
                    help='Show progress bars during experiments', action='store_true')

args = parser.parse_args()
