from neuralnets.resnet import ResNet


def prepare_experiment(name, datasource):
    if name == "resnet_pretrained_duplicate":
        return ResNet(datasource=datasource, pretrained=True, duplicatesamples=True, consecutivesamples=False, epochs_validate_multiplier=3)
    if name == "resnet_pretrained_random":
        return ResNet(datasource=datasource, pretrained=True, duplicatesamples=False, consecutivesamples=False)
    if name == "resnet_pretrained_consecutive":
        return ResNet(datasource=datasource, pretrained=True, duplicatesamples=False, consecutivesamples=True)
    if name == "resnet_untrained_duplicate":
        return ResNet(datasource=datasource, pretrained=False, duplicatesamples=True, consecutivesamples=False, epochs_validate_multiplier=3, epochs_validate_skip=1000)
    if name == "resnet_untrained_random":
        return ResNet(datasource=datasource, pretrained=False, duplicatesamples=False, consecutivesamples=False, epochs_validate_skip=750)
    if name == "resnet_untrained_consecutive":
        return ResNet(datasource=datasource, pretrained=False, duplicatesamples=False, consecutivesamples=True, epochs_validate_skip=1000)

    raise Exception("Could not find an experiment with the specified name!")
