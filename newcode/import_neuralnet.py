data_parsed_file = '../fma_medium_parsed/fma_medium_parsed'
data_output_file = '../output/'


if __name__ == '__main__':
    import pyfiglet
    ascii_banner = pyfiglet.figlet_format("GenreProject")
    print(ascii_banner, flush=True)
    print("", flush=True)

    import config.config as config
    import config.experiments as experiments
    print("Current settings:")
    print(config.args)
    print("", flush=True)
    print("", flush=True)

    from models.datasource import DataSource

    model_file = data_output_file + config.args.name + "_model.pth"
    parameter_file = data_output_file + config.args.name + "_parameters.pth"
    tracklist_file = data_output_file + config.args.name + ".tracklist"

    print("Preparing data source:", flush=True)
    datasource = DataSource(data_parsed_file + ".npy", data_parsed_file + ".csv")
    datasource.define_datacollection("train", 50)
    datasource.define_datacollection("test", 25)
    datasource.define_datacollection("validate", 25)
    datasource.load(tracklist_file)
    datasource.prepare(shuffle=False)
    print("Done!", flush=True)
    print("", flush=True)

    print("Preparing neural networks:", flush=True)
    neuralnet = experiments.prepare_experiment(config.args.experiment, datasource)
    neuralnet.load(model_file)
    print("Done!", flush=True)
    print("", flush=True)

    print("Extended testing neural networks:", flush=True)
    neuralnet.test_extended("test")
    print("Done!", flush=True)
    print("", flush=True)

    print("Saving neural networks:", flush=True)
    neuralnet.save(config.args.name)
    print("Done!", flush=True)
    print("", flush=True)

