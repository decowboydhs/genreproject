import numpy
import pandas
import math
import pickle
from models.datacollection import DataCollection
from models.track import Track
import config.config as config


class DataSource:
    def __init__(self, data_file, metadata_file):
        self.data_file = data_file
        self.metadata_file = metadata_file

        print("- Reading data files...", end='', flush=True)
        self.metadata = pandas.read_csv(self.metadata_file)
        if config.args.minimize_disk_io:
            self.data = numpy.load(self.data_file)
        else:
            self.data = numpy.load(self.data_file, mmap_mode='r')
        print(" 100%", flush=True)

        self.datacollections = {}
        self.classes = []
        self.track_list = None

    def define_datacollection(self, name, size):
        print("- Defining data collection '" + name + "' of size '" + str(size) + "%'...", end='', flush=True)
        self.datacollections[name] = DataCollection(name, size)
        print(" 100%", flush=True)

    def get_datacollection(self, name):
        return self.datacollections[name]

    def load(self, file):
        file = open(file, 'rb')
        self.track_list = pickle.load(file)
        file.close()

    def prepare(self, shuffle=True):
        if len(self.datacollections) == 0:
            raise Exception("Data source has no data collections!")

        total = 0
        for name, datacollection in self.datacollections.items():
            total += datacollection.size

        if total > 100:
            raise Exception("Data source contain data collections that want more than 100% of data!")

        if self.track_list is None:
            self.track_list = self.metadata['track_id'].unique()

        sample_count = len(self.metadata.index)

        self.classes = sorted(self.metadata['genre'].unique())

        if shuffle:
            print("- Shuffling data...", end='', flush=True)
            numpy.random.shuffle(self.track_list)
            print(" 100%", flush=True)

        class_trackcount = numpy.zeros((len(self.classes)), dtype=numpy.uint32)
        for index, genre in enumerate(self.classes):
            class_trackcount[index] = len(self.metadata.loc[self.metadata.genre == genre]['track_id'].unique())

        position = 0
        total = len(self.track_list)
        last_progress = -1

        print("- Distributing data over data collections...", end='', flush=True)
        current_class_trackcount = numpy.zeros((len(self.classes)), dtype=numpy.uint32)
        for track_id in self.track_list:
            rows = self.metadata.loc[self.metadata.track_id == track_id]

            data_start = rows.head(1)['position'].item()
            data_end = rows.tail(1)['position'].item()
            segment_count = len(rows.index)
            genre = rows.head(1)['genre'].item()

            genre_index = self.classes.index(genre)
            track_position = current_class_trackcount[genre_index] * 100.0 / class_trackcount[genre_index]

            datacollection_offset = 0
            for name, datacollection in self.datacollections.items():
                if track_position >= datacollection_offset and track_position < datacollection_offset + datacollection.size:
                    datacollection.tracks.append(Track(segment_count=segment_count, data_start=data_start, data_end=data_end, track_id=track_id, genre=genre))
                    break
                datacollection_offset += datacollection.size

            current_class_trackcount[genre_index] += 1
            position += 1

            if config.args.show_progress:
                progress = math.floor(position / total * 100)

                if progress % 4 == 0 and progress > last_progress:
                    last_progress = progress
                    print(".", end='', flush=True)

        print(" 100%", flush=True)

