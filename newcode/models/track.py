class Track:
    def __init__(self, segment_count, data_start, data_end, track_id, genre):
        self.segment_count = segment_count
        self.data_start = data_start
        self.data_end = data_end
        self.track_id = track_id
        self.genre = genre
